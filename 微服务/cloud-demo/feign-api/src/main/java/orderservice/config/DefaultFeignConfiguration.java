package orderservice.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;

/**
 * OpenFeign的配置
 */
public class DefaultFeignConfiguration {

    /**
     * 配置日志基本
     * @return
     */
    @Bean
    public Logger.Level feignLogLevel(){
        return Logger.Level.BASIC; // 日志级别为BASIC
    }
}
