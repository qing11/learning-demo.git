package orderservice.feign;

import orderservice.config.DefaultFeignConfiguration;
import orderservice.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "userservice",configuration = DefaultFeignConfiguration.class) //userservice：用户服务的服务名称
public interface UserClients {

    @GetMapping("/user/{id}")
    User findById(@PathVariable("id") Long id);
}
