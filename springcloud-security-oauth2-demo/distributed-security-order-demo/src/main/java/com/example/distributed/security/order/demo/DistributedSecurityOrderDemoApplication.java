package com.example.distributed.security.order.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistributedSecurityOrderDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistributedSecurityOrderDemoApplication.class, args);
	}

}
