package com.example.distributed.security.uaa.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class DistributedSecurityUaaDemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(DistributedSecurityUaaDemoApplication.class, args);
	}

}
