package com.example.distributed.security.gateway.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistributedSecurityGatewayDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistributedSecurityGatewayDemoApplication.class, args);
	}

}
