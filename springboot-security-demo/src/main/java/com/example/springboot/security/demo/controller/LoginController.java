package com.example.springboot.security.demo.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class LoginController {

    /**
     * 进入到登录页面
     *
     * @return
     */
    @GetMapping("/login-view.html")
    public String loginView() {
        return "login";
    }


    /**
     * 登录成功后，调用的接口
     * @return
     */
    @GetMapping(value = "/login-success")
    public String loginSuccess(Model model) {
        model.addAttribute("name", getUsername() + " 登录成功");
        return "index";
    }

    @ResponseBody
    @GetMapping( "/view")
    @PreAuthorize("hasAuthority('sysUserInfo:view')")
    public String view() {
        return getUsername() + " 有查看权限";
    }

    /**
     * 登录后就可以访问
     * @return
     */
    @ResponseBody
    @GetMapping("/update")
    @PreAuthorize("isAuthenticated()")
    public String update() {
        return getUsername() + " 有编辑权限";
    }

    @ResponseBody
    @GetMapping("/delete")
    @PreAuthorize("isAnonymous()")
    public String delete() {
        return getUsername() + " 有删除权限";
    }

    //获取当前用户信息
    private String getUsername() {
        String username = null;
        //当前认证通过的用户身份
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        //用户身份
        Object principal = authentication.getPrincipal();
        if (principal == null) {
            username = "匿名";
        }
        if (principal instanceof org.springframework.security.core.userdetails.UserDetails) {
            UserDetails userDetails = (UserDetails) principal;
            username = userDetails.getUsername();
        } else {
            username = principal.toString();
        }
        return username;
    }
}
