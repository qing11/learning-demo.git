package com.example.springboot.security.demo.handle;

import com.example.springboot.security.demo.common.JsonData;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by linziyu on 2019/2/9.
 *
 * 用户验证失败处理类
 */
@Slf4j
@Component("UserLoginAuthenticationFailureHandler")
public class UserLoginAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {



    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {

        String username = (String) request.getAttribute("username");
        System.out.println("登录用户:"+username);

        JsonData jsonData = null;

        /**
         * UserDetailsService 校验用户时,主动抛出的错误,如:用户信息不存在 用户被禁用等
         */
        if (exception instanceof UsernameNotFoundException){
            jsonData = new JsonData(402,exception.getMessage());
        }
        /**
         * 密码错误
         */
        if (exception instanceof BadCredentialsException){
            log.info(exception.getMessage());
            jsonData = new JsonData(403,"账号或密码错误");

            // String user_name = userService.findByUserNameAttemps(username);
            // if (user_name == null){
            //     String time = DateUtil.getTimeToString();
            //     UserLoginAttempts userLoginAttempts = new UserLoginAttempts(username,1,time);
            //     userService.saveAttempts(userLoginAttempts);
            // }


            // if(userService.getAttempts(username) == 1){
            //     String time = DateUtil.getTimeToString();
            //     userService.setAttempts(username,time);
            //     jsonData = new JsonData(403,"密码错误,你还有2次机会进行登录操作");
            // }
            // else if(userService.getAttempts(username) == 3){
            //     User user = userService.findByUserName(username);
            //     userService.LockUser(user.getId());
            //     jsonData = new JsonData(403,"最后一次尝试登陆失败，你已经被冻结了");
            // }
            // else if (userService.getAttempts(username) ==2 ){
            //     String time = DateUtil.getTimeToString();
            //     userService.setAttempts(username,time);
            //     jsonData = new JsonData(403,"密码错误,你还有1次机会进行登录操作");
            // }
        }

        String json = new Gson().toJson(jsonData);
        response.setContentType("application/json;charset=utf-8");
        PrintWriter out = response.getWriter();

        out.write(json);
        out.flush();
        out.close();
    }
}
