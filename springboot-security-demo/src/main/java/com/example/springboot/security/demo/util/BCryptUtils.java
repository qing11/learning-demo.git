package com.example.springboot.security.demo.util;

import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * Spring Security BCrypt 加密工具类
 */
public class BCryptUtils {
    /**
     * 对密码进行加密
     * @param password
     * @return
     */
    public static String encryption(String password){
        String hashpw = BCrypt.hashpw(password, BCrypt.gensalt());
        return hashpw;
    }

    /**
     * 密码校验
     * @param plainPassword 原密码
     * @param hashpw 加密后的密码
     * @return
     */
    public static boolean checkpw(String plainPassword, String hashpw){
        boolean checkpw = BCrypt.checkpw(plainPassword, hashpw);
        return checkpw;
    }

    public static void main(String[] args) {
        String all = BCryptUtils.encryption("secret");
        System.out.println(all);
    }
}
