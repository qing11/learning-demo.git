package com.example.springboot.security.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 视图配置类
 * @author Administrator
 * @version 1.0
 **/
@Configuration
public class WebConfig implements WebMvcConfigurer {


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //请求 /  时，跳转到 login-view.html
        registry.addViewController("/").setViewName("redirect:/login-view.html");
        //请求 /login-view.html 时，还是跳转到 login-view.html
        registry.addViewController("/login-view.html").setViewName("login-view");
    }

}
