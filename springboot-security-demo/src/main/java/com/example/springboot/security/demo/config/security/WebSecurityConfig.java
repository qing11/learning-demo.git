package com.example.springboot.security.demo.config.security;

import com.example.springboot.security.demo.handle.UserAuthenticationAccessDeniedHandler;
import com.example.springboot.security.demo.handle.UserLoginAuthenticationFailureHandler;
import com.example.springboot.security.demo.handle.UserLoginAuthenticationSuccessHandler;
import com.example.springboot.security.demo.handle.UserLogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;

/**
 * 配置 Security
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)//开启security的权限校验注解
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 用户验证成功处理类
     */
    @Resource
    private UserLoginAuthenticationSuccessHandler userLoginAuthenticationSuccessHandler;

    /**
     * 用户验证失败处理类
     */
    @Resource
    private UserLoginAuthenticationFailureHandler userLoginAuthenticationFailureHandler;

    /**
     * 无权限操作时的处理类
     */
    @Resource
    private UserAuthenticationAccessDeniedHandler userAuthenticationAccessDeniedHandler;

    /**
     * 用户登出处理类
     */
    @Resource
    private UserLogoutSuccessHandler userLogoutSuccessHandler;

    //密码编码器
    @Bean
    public PasswordEncoder passwordEncoder(){
        //使用BCrypt进行密码加密校验
        return new BCryptPasswordEncoder();
    }

    /**
     * 用户登录认证
     * @return
     */
    @Bean
    public UserDetailsServiceImpl userDetailsService(){
        return new UserDetailsServiceImpl();
    }


    //配置安全拦截机制
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();//禁用CSRF控制，即spring security不再限制CSRF，即跨越访问

        http
                .authorizeRequests()
                .antMatchers("/static/**")//静态资源等不需要验证
                .permitAll()//不需要身份认证
                .anyRequest().authenticated();//其他路径必须验证身份
        http
                .formLogin()
                .loginPage("/login-view.html")//登录页面，加载登录的html页面
                .loginProcessingUrl("/login")//发送Ajax请求的路径
                .usernameParameter("username")//请求验证参数
                .passwordParameter("password")//请求验证参数
                .successHandler(userLoginAuthenticationSuccessHandler)//验证成功处理
                .failureHandler(userLoginAuthenticationFailureHandler)//验证失败处理
                .permitAll();//登录页面无需设置验证

        http
                .logout()
                .logoutSuccessHandler(userLogoutSuccessHandler)//登出处理
                .permitAll()//不需要身份认证
                .and()
                .exceptionHandling().accessDeniedHandler(userAuthenticationAccessDeniedHandler);//无权限时的处理
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService());
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        //设置hideUserNotFoundExceptions为false
        daoAuthenticationProvider.setHideUserNotFoundExceptions(false);
        return daoAuthenticationProvider;
    }
}
