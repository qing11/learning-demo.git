package com.example.springboot.security.demo.config.security;

import com.example.springboot.security.demo.model.User;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * spring security 的登录实现
 */
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //登录账号
        System.out.println("username="+username);
        //1、这里是从数据库查询出来的对象
        User user = new User();
        user.setUsername(username);
        //这个加密密码是456
        user.setPassword("$2a$10$zgnAvX32nq.NaWtQ0SrMGOiJUH4jtTCXtLHWPPWBnHP4knzndbROm");

        //保存用户账号到session中
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        request.setAttribute("username",username);

        //模拟用户查询出来有问题
        if (username.equals("zhangsan")){
           throw new UsernameNotFoundException("用户不存在");
        }
        if (username.equals("zhangsan2")){
            throw new UsernameNotFoundException("用户被禁用");
        }

        //从数据库中查询用户的权限信息
        List<String> permissions = new ArrayList<>();
        permissions.add("sysUserInfo:view");
        permissions.add("sysUserInfo:update");
        permissions.add("sysUserInfo:delete");
        user.setPermissionList(permissions);

        //如果不重写UserDetails，那么可以用下面的这种写法
//        String[] permissionArray = new String[permissions.size()];
//        permissions.toArray(permissionArray);
//        UserDetails userDetails = org.springframework.security.core.userdetails.User.withUsername(username).password("123").authorities(permissionArray).build();

        return user;
    }
}
